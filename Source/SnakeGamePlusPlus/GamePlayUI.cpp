#include "GamePlayUI.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"
#include "PlayerPawnBase.h"
#include "SnakeGamePlusPlusGameModeBase.h"

bool UGamePlayUI::Initialize()
{
	Super::Initialize();

	pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));	

	scoreText->SetText(FText::FromString("Score"));

	scoreText->TextDelegate.BindUFunction(this, "SetScoreField");
	pause->OnClicked.AddDynamic(this, &UGamePlayUI::SetGamePause);

	return true;
}

FText UGamePlayUI::SetScoreField()
{
	return FText::FromString(FString::FromInt(pawn->GetScore()));
}

void UGamePlayUI::SetGamePause()
{
	ASnakeGamePlusPlusGameModeBase* gameMode = Cast<ASnakeGamePlusPlusGameModeBase>(GetWorld()->GetAuthGameMode());
	if (IsValid(gameMode))
	{
		gameMode->Pause(true);
	}
	OnPauseButtonClick();
}
