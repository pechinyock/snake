#pragma once

#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FruitBase.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API AFruitBase : public AActor, public IInteractable

{
	GENERATED_BODY()
	
public:	
	AFruitBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* mesh;

	UPROPERTY()
	UMaterialInterface* material;

	TSubclassOf<AFruitBase> fruitClass;

	float step;	

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* other) override;

	UFUNCTION()
	FLinearColor GetColour();

private:

	UPROPERTY()
	FLinearColor myColour;

	UFUNCTION()
	FLinearColor MakeRandomColour();
};
