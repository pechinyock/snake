#include "MovementButtons.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "Components/Button.h"
#include "SnakeHeadBase.h"

bool UMovementButtons::Initialize()
{
	Super::Initialize();	

	playerPawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));	

	buttonMoveUp->OnClicked.AddDynamic(this, &UMovementButtons::MoveUp);
	buttonMoveDown->OnClicked.AddDynamic(this, &UMovementButtons::MoveDown);
	buttonMoveLeft->OnClicked.AddDynamic(this, &UMovementButtons::MoveLeft);
	buttonMoveRight->OnClicked.AddDynamic(this, &UMovementButtons::MoveRight);

	score = 0;

	return true;
}


void UMovementButtons::MoveUp()
{
	if (!playerPawn->snakeHead->isMoved)
	{
		if (playerPawn->snakeHead->lastMovementDirection != EMovementDirection::DOWN && playerPawn->snakeHead->lastMovementDirection != EMovementDirection::UP)
		{
			playerPawn->snakeHead->movementDirection = FVector(1, 0, 0);
			playerPawn->snakeHead->lastMovementDirection = EMovementDirection::UP;
			playerPawn->snakeHead->isMoved = true;
		}
	}
}

void UMovementButtons::MoveDown()
{
	if (!playerPawn->snakeHead->isMoved)
	{
		if (playerPawn->snakeHead->lastMovementDirection != EMovementDirection::UP && playerPawn->snakeHead->lastMovementDirection != EMovementDirection::DOWN)
		{
			playerPawn->snakeHead->movementDirection = FVector(-1, 0, 0);
			playerPawn->snakeHead->lastMovementDirection = EMovementDirection::DOWN;
			playerPawn->snakeHead->isMoved = true;
		}
	}
}

void UMovementButtons::MoveLeft()
{
	if (!playerPawn->snakeHead->isMoved)
	{
		if (playerPawn->snakeHead->lastMovementDirection != EMovementDirection::RIGHT && playerPawn->snakeHead->lastMovementDirection != EMovementDirection::LEFT)
		{
			playerPawn->snakeHead->movementDirection = FVector(0, -1, 0);
			playerPawn->snakeHead->lastMovementDirection = EMovementDirection::LEFT;
			playerPawn->snakeHead->isMoved = true;
		}
	}
}

void UMovementButtons::MoveRight()
{
	if (!playerPawn->snakeHead->isMoved)
	{
		if (playerPawn->snakeHead->lastMovementDirection != EMovementDirection::LEFT && playerPawn->snakeHead->lastMovementDirection != EMovementDirection::RIGHT)
		{
			playerPawn->snakeHead->movementDirection = FVector(0, 1, 0);
			playerPawn->snakeHead->lastMovementDirection = EMovementDirection::RIGHT;
			playerPawn->snakeHead->isMoved = true;
		}
	}
}
