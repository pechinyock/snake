#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeHeadBase.generated.h"

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAMEPLUSPLUS_API ASnakeHeadBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASnakeHeadBase();

	UPROPERTY(EditDefaultsOnly)
	float forceToDie;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* mesh;

	UPROPERTY()
	bool isAlive;

	UPROPERTY()
	class AGameFieldBase* gameField;

	UPROPERTY()
	EMovementDirection lastMovementDirection;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ASnakeElementBase> snakeElementClass;

	UPROPERTY()
	FVector newSnakeElementSpawnLocation;

	UPROPERTY(EditDefaultsOnly)
	float elementSize;

	UPROPERTY()
	class APlayerPawnBase* pawn;

	static float movementSpeed;

	UPROPERTY()
	bool isMoved;
	
	UPROPERTY()
	bool isCounting;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	FVector movementDirection;

	UFUNCTION()
	void Move();

	UFUNCTION()
	void AddSnakeElememnt(int elementsCount = 1);

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
							AActor* otherActor,
							UPrimitiveComponent* otherComponent,
							int32 otherBodyIndex, bool bFromSweep,
							const FHitResult& sweepResult);
	
	UFUNCTION()
	int GetSnakeElementsCount();

	UFUNCTION()
	void OnDie();

	UFUNCTION()		      
	void OnPickUpCubix();

	static float GetMovementSpeed();

	UFUNCTION()
	TArray<FVector> GetSnakeLocation();

private:

	UPROPERTY()
	UMaterialInterface* material;

	UPROPERTY()
	TArray<class ASnakeElementBase*> snakeElements;

	UPROPERTY()
	TArray<FVector> snakeLocation;

	UFUNCTION()
	float GetRandomRotation();

};
