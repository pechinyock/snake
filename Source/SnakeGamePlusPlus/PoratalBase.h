#pragma once

#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PoratalBase.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API APoratalBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	APoratalBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USkeletalMeshComponent* skelet1;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USkeletalMeshComponent* skelet2;


	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<APoratalBase> portalClass;

	UPROPERTY()
	APoratalBase* wayout;

	UPROPERTY()
	FVector teleportTo;

	float snakeMovementSpeed;

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY()
	int stepsToDelete;

	UPROPERTY()
	bool startingCountToDelete;

	UFUNCTION()
	void CountToDelete();

public:	

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void Interact(AActor* other) override;

private:

	UPROPERTY()
	class ASnakeHeadBase* teleportingSnake;

	UPROPERTY()
	FTimerHandle destroyTimer;

	int stepsToDestroy;

	int fstTimeCheckElementsCount;

	int timerTicks;
};
