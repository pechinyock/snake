#pragma once

#include "Interactable.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeElementBase.generated.h"


UCLASS()
class SNAKEGAMEPLUSPLUS_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:
	ASnakeElementBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)	
	class UStaticMeshComponent* mesh;

	UPROPERTY()
	UMaterialInterface* material;

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* other) override;

	UFUNCTION()
	void SetColour(FLinearColor newColour);

private:	
	UPROPERTY()
	UMaterialInstanceDynamic* dynamicMaterial;
};
