#include "FruitBase.h"
#include "SnakeHeadBase.h"
#include "Components/StaticMeshComponent.h"

AFruitBase::AFruitBase()
{
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	mesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	step = 0;

	RootComponent = mesh;
}

void AFruitBase::BeginPlay()
{
	Super::BeginPlay();

	material = mesh->GetMaterial(0);
	UMaterialInstanceDynamic* dynamicMaterial = UMaterialInstanceDynamic::Create(material, this);

	mesh->SetMaterial(0, dynamicMaterial);
	myColour = MakeRandomColour();
	dynamicMaterial->SetVectorParameterValue(TEXT("Colour"), myColour);
}

void AFruitBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	step += 0.4f;
	SetActorRotation(FRotator(0, step, 0));
}

void AFruitBase::Interact(AActor* other)
{
	ASnakeHeadBase* snakeHead = Cast<ASnakeHeadBase>(other);
	if (IsValid(snakeHead))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Hit fruit"))
		snakeHead->AddSnakeElememnt();
		this->Destroy();
	}
}

FLinearColor AFruitBase::GetColour()
{
	return myColour;
}

FLinearColor AFruitBase::MakeRandomColour()
{
	int randomInt = FMath::RandRange(0, 2);

	switch (randomInt)
	{
	case 0:
		return FLinearColor(0.86f, 0.f, 0.001f, 1.f);
		break;
	case 1:
		return FLinearColor(0.f, 0.17f, 0.f, 1.f);
		break;
	case 2:		
		return FLinearColor(0.f, 0.17f, 0.86f, 1.f);
		break;
	}
	return FLinearColor::Black;
}

