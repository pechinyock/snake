#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "GameFieldBase.h"
#include "SnakeHeadBase.h"
#include "PoratalBase.h"


APlayerPawnBase::APlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	pawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	SetRootComponent(pawnCamera);

	score = 0;
}

void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	SetActorLocation(cameraPosition);
	CreateSnakeHeadActor();
	SpawnGameField();
	MoveRight();
	score = 0;
}

void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	ensure(PlayerInputComponent);
	
	PlayerInputComponent->BindAction("MoveUp", IE_Pressed, this, &APlayerPawnBase::MoveUp);
	PlayerInputComponent->BindAction("MoveDown", IE_Pressed, this, &APlayerPawnBase::MoveDown);
	PlayerInputComponent->BindAction("MoveLeft", IE_Pressed, this, &APlayerPawnBase::MoveLeft);
	PlayerInputComponent->BindAction("MoveRight", IE_Pressed, this, &APlayerPawnBase::MoveRight);
}

void APlayerPawnBase::CreateSnakeHeadActor()
{
	snakeHead = GetWorld()->SpawnActor<ASnakeHeadBase>(snakeHeadClass, FVector(880, 880, 60), FRotator(0, 0, 0));		
}

void APlayerPawnBase::SpawnGameField()
{
	gameField = GetWorld()->SpawnActor<AGameFieldBase>(gameFieldClass, FVector(790, 750, -50), FRotator(0, 0, 0));
	snakeHead->gameField = GetGameField();
}

ASnakeHeadBase* APlayerPawnBase::GetSnakeHead()
{
	return snakeHead;
}

AGameFieldBase* APlayerPawnBase::GetGameField()
{
	return gameField;
}

void APlayerPawnBase::MoveUp()
{	
	if (!snakeHead->isMoved)
	{
		if (snakeHead->lastMovementDirection != EMovementDirection::DOWN && snakeHead->lastMovementDirection != EMovementDirection::UP)
		{
			snakeHead->movementDirection = FVector(1, 0, 0);
			snakeHead->lastMovementDirection = EMovementDirection::UP;
			snakeHead->isMoved = true;			
		}
	}	
}

void APlayerPawnBase::MoveDown()
{
	if (!snakeHead->isMoved)
	{
		if (snakeHead->lastMovementDirection != EMovementDirection::UP && snakeHead->lastMovementDirection != EMovementDirection::DOWN)
		{
			snakeHead->movementDirection = FVector(-1, 0, 0);
			snakeHead->lastMovementDirection = EMovementDirection::DOWN;
			snakeHead->isMoved = true;
		}
	}
}

void APlayerPawnBase::MoveLeft()
{
	if (!snakeHead->isMoved)
	{
		if (snakeHead->lastMovementDirection != EMovementDirection::RIGHT && snakeHead->lastMovementDirection != EMovementDirection::LEFT)
		{
			snakeHead->movementDirection = FVector(0, -1, 0);
			snakeHead->lastMovementDirection = EMovementDirection::LEFT;
			snakeHead->isMoved = true;
		}
	}
}

void APlayerPawnBase::MoveRight()
{
	if (!snakeHead->isMoved)
	{
		if (snakeHead->lastMovementDirection != EMovementDirection::LEFT && snakeHead->lastMovementDirection != EMovementDirection::RIGHT)
		{
			snakeHead->movementDirection = FVector(0, 1, 0);
			snakeHead->lastMovementDirection = EMovementDirection::RIGHT;
			snakeHead->isMoved = true;
		}
	}
}

void APlayerPawnBase::SetScore(int32 value)
{
	score += value;
	if (score % 4 == 0) 
	{
		//gameField->RebuildGameField();		
	}	
}

int32 APlayerPawnBase::GetScore()
{
	return score;
}
