#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MovementButtons.generated.h"


UCLASS()
class SNAKEGAMEPLUSPLUS_API UMovementButtons : public UUserWidget
{
	GENERATED_BODY()
public:

	UPROPERTY()
	class APlayerPawnBase* playerPawn;

protected:

	UPROPERTY(meta = (BindWidget))
	class UButton* buttonMoveUp;

	UPROPERTY(meta = (BindWidget))
	class UButton* buttonMoveDown;

	UPROPERTY(meta = (BindWidget))
	class UButton* buttonMoveLeft;

	UPROPERTY(meta = (BindWidget))
	class UButton* buttonMoveRight;

	UPROPERTY()
	int score;

	UFUNCTION()
	void MoveUp();

	UFUNCTION()
	void MoveDown();

	UFUNCTION()
	void MoveLeft();

	UFUNCTION()
	void MoveRight();

private:
	virtual bool Initialize();
};
