// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGamePlusPlus.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGamePlusPlus, "SnakeGamePlusPlus" );
