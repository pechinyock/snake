#include "PauseMenu.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "SnakeGamePlusPlusGameModeBase.h"


bool UPauseMenu::Initialize()
{
	Super::Initialize();

	pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	restart->OnClicked.AddDynamic(this, &UPauseMenu::RestartGame);
	resume->OnClicked.AddDynamic(this, &UPauseMenu::ResuemGame);
	quit->OnClicked.AddDynamic(this, &UPauseMenu::QuitGame);

	return true;
}

void UPauseMenu::RestartGame()
{
	ASnakeGamePlusPlusGameModeBase* gameMode = Cast<ASnakeGamePlusPlusGameModeBase>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		gameMode->RestartLevel();
	}
}

void UPauseMenu::ResuemGame()
{
	ASnakeGamePlusPlusGameModeBase* gameMode = Cast<ASnakeGamePlusPlusGameModeBase>(GetWorld()->GetAuthGameMode());
	if (IsValid(gameMode))
	{		
		gameMode->Pause(false);
	}
	OnResuemButtonClick();
}

void UPauseMenu::QuitGame()
{
	APlayerController* SpecificPlayer = GetWorld()->GetFirstPlayerController();
	UKismetSystemLibrary::QuitGame(GetWorld(), SpecificPlayer, EQuitPreference::Quit, true);
}
