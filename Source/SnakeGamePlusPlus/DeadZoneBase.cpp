#include "DeadZoneBase.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeHeadBase.h"
#include "SnakeGamePlusPlusGameModeBase.h"

ADeadZoneBase::ADeadZoneBase()
{
	PrimaryActorTick.bCanEverTick = true;
	
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	mesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = mesh;
}

void ADeadZoneBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void ADeadZoneBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADeadZoneBase::Interact(AActor* other)
{
	ASnakeHeadBase* snakeHead = Cast<ASnakeHeadBase>(other);
	ASnakeGamePlusPlusGameModeBase* gameMode = Cast<ASnakeGamePlusPlusGameModeBase>(GetWorld()->GetAuthGameMode());

	if (IsValid(snakeHead))
	{
		snakeHead->OnDie();
			
		if (IsValid(gameMode))
		{
			//gameMode->GameOver();
		}
	}
}

