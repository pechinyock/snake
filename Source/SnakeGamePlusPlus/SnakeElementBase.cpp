#include "SnakeElementBase.h"
#include "PoratalBase.h"
#include "SnakeHeadBase.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeGamePlusPlusGameModeBase.h"

ASnakeElementBase::ASnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	mesh->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = mesh;
}

void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();	

	material = mesh->GetMaterial(0);
    dynamicMaterial = UMaterialInstanceDynamic::Create(material, this);
	mesh->SetMaterial(0, dynamicMaterial);	
}

void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASnakeElementBase::Interact(AActor* other)
{
	ASnakeHeadBase* snakeHead = Cast<ASnakeHeadBase>(other);

	if (IsValid(snakeHead))
	{
		snakeHead->OnDie();				
	}
}

void ASnakeElementBase::SetColour(FLinearColor newColour)
{
	if (IsValid(dynamicMaterial))
	{
		//dynamicMaterial->SetVectorParameterValue(TEXT("Colour"), newColour);
	}
}

