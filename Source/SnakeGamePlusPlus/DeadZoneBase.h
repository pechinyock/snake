#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "DeadZoneBase.generated.h"


UCLASS()
class SNAKEGAMEPLUSPLUS_API ADeadZoneBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ADeadZoneBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* mesh;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void Interact(AActor* other) override;

};
