#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:

	APlayerPawnBase();

	UPROPERTY(EditDefaultsOnly)
	FVector cameraPosition;

	UPROPERTY()
	class UMovementButtons* playerButtons;
	
	UPROPERTY(BlueprintReadWrite)
	class UCameraComponent* pawnCamera;

	UPROPERTY(BlueprintReadWrite)
	class ASnakeHeadBase* snakeHead;

	UPROPERTY(BlueprintReadWrite)
	class AGameFieldBase* gameField;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AGameFieldBase> gameFieldClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeHeadBase> snakeHeadClass;


protected:
	virtual void BeginPlay() override;


public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void CreateSnakeHeadActor();

	UFUNCTION()
	void SpawnGameField();

	UFUNCTION(BlueprintPure)
	ASnakeHeadBase* GetSnakeHead();

	UFUNCTION()
	AGameFieldBase* GetGameField();

	UFUNCTION()
	void MoveUp();

	UFUNCTION()
	void MoveDown();

	UFUNCTION()
	void MoveLeft();

	UFUNCTION()
	void MoveRight();

	UFUNCTION(BlueprintCallable)
	void SetScore(int32 value);

	UFUNCTION()
	int32 GetScore();

private: 

	UPROPERTY()
	int32 score;
};
