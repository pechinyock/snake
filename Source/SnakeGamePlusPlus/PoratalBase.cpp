#include "PoratalBase.h"
#include "Interactable.h"
#include "SnakeHeadBase.h"
#include "SnakeElementBase.h"
#include "Components/StaticMeshComponent.h"
#include "GameFieldBase.h"
#include "Components/SkeletalMeshComponent.h"

APoratalBase::APoratalBase()
{
	PrimaryActorTick.bCanEverTick = true;

	skelet1 = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh1"));
	skelet1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	skelet1->SetGenerateOverlapEvents(true);
	skelet1->SetCollisionResponseToAllChannels(ECR_Overlap);

	skelet2 = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh2"));


	RootComponent = skelet1;

	skelet2->SetupAttachment(skelet1);

	snakeMovementSpeed = ASnakeHeadBase::GetMovementSpeed();
}

void APoratalBase::BeginPlay()
{
	Super::BeginPlay();	
	SetActorTickInterval(snakeMovementSpeed);
	startingCountToDelete = false;	
	stepsToDestroy = 4;
	timerTicks = 0;
}

void APoratalBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);		
}

void APoratalBase::Interact(AActor* other)
{
	UE_LOG(LogTemp, Warning, TEXT("BeginOverlap"))
	teleportingSnake = Cast<ASnakeHeadBase>(other);

	if (IsValid(teleportingSnake))
	{						
		fstTimeCheckElementsCount = stepsToDestroy = teleportingSnake->GetSnakeElementsCount();
		teleportingSnake->SetActorLocation(teleportTo + (teleportingSnake->movementDirection * FVector(110.f, 110.f, 1.f)));		
		GetWorld()->GetTimerManager().SetTimer(destroyTimer, this, &APoratalBase::CountToDelete, snakeMovementSpeed, true , 0.f);
	}
}

void APoratalBase::CountToDelete()
{	
	if (teleportingSnake != nullptr)
	{
		if (teleportingSnake->isAlive)
		{
			if (fstTimeCheckElementsCount < teleportingSnake->GetSnakeElementsCount())
			{
				int difference = teleportingSnake->GetSnakeElementsCount() - fstTimeCheckElementsCount;
				stepsToDestroy += difference;
				fstTimeCheckElementsCount = teleportingSnake->GetSnakeElementsCount();
			}

			stepsToDestroy--;
			if (stepsToDestroy < 0)
			{
				this->Destroy();
				wayout->Destroy();
				AGameFieldBase* currentGameField = Cast<AGameFieldBase>(teleportingSnake->gameField);
				if (IsValid(currentGameField))
				{
					currentGameField->GotPortalAt(this->GetActorLocation(), wayout->GetActorLocation());
				}
			}
		}
	}				
}


