#include "SnakeGamePlusPlusGameModeBase.h"
#include "Kismet/GameplayStatics.h"


void ASnakeGamePlusPlusGameModeBase::StartPlay()
{
	Super::StartPlay();
	//UE_LOG(LogTemp, Warning, TEXT("The game is started"))		
}

void ASnakeGamePlusPlusGameModeBase::GameOver()
{
	UE_LOG(LogTemp, Warning, TEXT("The game is over"))
	GetWorld()->GetTimerManager().SetTimer(timer, this, &ASnakeGamePlusPlusGameModeBase::RestartLevel, 3.f, false, 3.f);
	
	//UE_LOG(LogTemp, Warning, TEXT("The game is over"))
	//UGameplayStatics::SetGamePaused(this, true);
}

void ASnakeGamePlusPlusGameModeBase::RestartLevel()
{
	UE_LOG(LogTemp, Warning, TEXT("restart"))	
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void ASnakeGamePlusPlusGameModeBase::Pause(bool bIsPaused)
{	
	APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	if (MyPlayer != nullptr)
	{
		MyPlayer->SetPause(bIsPaused);
	}	
}
