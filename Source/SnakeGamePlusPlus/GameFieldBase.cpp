#include "GameFieldBase.h"
#include "SnakeHeadBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"
#include "FruitBase.h"
#include "DeadZoneBase.h"
#include "PoratalBase.h"

AGameFieldBase::AGameFieldBase()
{
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	RootComponent = mesh;
	
	maxExistingPortal = 1;
	existingPortal = 0;

	maxExistingFruit = 4;
	existingFruit = 0;
}

void AGameFieldBase::BeginPlay()
{
	Super::BeginPlay();	

	borderX = borderY = 15;

	FillLocations(borderX, borderY);
	//SpawnBorder(borderX, borderY);

	APlayerPawnBase* player = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));	

	if (player)
	{		
		snakeHead = Cast<ASnakeHeadBase>(player->GetSnakeHead());
	}
	
	SpawnFruits();
	SpawnPortals();
	//GetWorld()->GetTimerManager().SetTimer(timer, this ,&AGameFieldBase::SpawnPortals, 10.f, true, 1.f);		
}

void AGameFieldBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGameFieldBase::SpawnBorder(int x, int y, int startX, int startY, float cellSize)
{		
	if (obstacles.Num() <= 0)
	{
		if (deadZoneClass)
		{
			for (int i = -1; i < y; i++)
			{
				obstacles.Add(GetWorld()->SpawnActor<ADeadZoneBase>(deadZoneClass, FVector(i * cellSize, -110.f, 60.f), FRotator(0, 0, 0)));
			}
			for (int i = 0; i < y; i++)
			{
				obstacles.Add(GetWorld()->SpawnActor<ADeadZoneBase>(deadZoneClass, FVector(i * cellSize, x * cellSize, 60.f), FRotator(0, 0, 0)));
			}
			for (int i = -1; i < x + 1; i++)
			{
				obstacles.Add(GetWorld()->SpawnActor<ADeadZoneBase>(deadZoneClass, FVector(y * cellSize, i * cellSize, 60.f), FRotator(0, 0, 0)));
			}
			for (int i = 0; i < x + 1; i++)
			{
				obstacles.Add(GetWorld()->SpawnActor<ADeadZoneBase>(deadZoneClass, FVector(-110.f, i * cellSize, 60.f), FRotator(0, 0, 0)));
			}
		}
	}			
}

void AGameFieldBase::SpawnPortals()
{	
	if (existingPortal <= 0)
	{
		if ((locations.Num() - existingObjectLocations.Num()) - snakeHead->GetSnakeElementsCount() > 5)
		{
			while (existingPortal < maxExistingPortal)
			{
				FVector rndLoc = GetRandomPosition();

				APoratalBase* firstPortal = GetWorld()->SpawnActor<APoratalBase>(portalClass, rndLoc, FRotator(0, 0, 0));

				firstPortal = Cast<APoratalBase>(firstPortal);

				existingObjectLocations.Add(rndLoc);

				FVector rndLocWayout = GetRandomPosition();

				APoratalBase* secPortal = GetWorld()->SpawnActor<APoratalBase>(portalClass, rndLocWayout, FRotator(0, 0, 0));

				secPortal = Cast<APoratalBase>(secPortal);

				existingObjectLocations.Add(rndLocWayout);

				firstPortal->wayout = secPortal;
				secPortal->wayout = firstPortal;

				firstPortal->teleportTo = secPortal->GetActorLocation();
				secPortal->teleportTo = firstPortal->GetActorLocation();

				existingPortal++;
			}
		}
	}
}

void AGameFieldBase::RebuildGameField()
{
	if (obstacles.Num() > 0)
	{
		for (int32 i = 0; i < obstacles.Num(); i++)
		{
			obstacles[i]->Destroy();
		}
	}
	obstacles.Empty();	
	
}

void AGameFieldBase::SpawnFruits()
{	
	if (existingFruit <= 0)
	{
		if ((locations.Num() - existingObjectLocations.Num()) - snakeHead->GetSnakeElementsCount() > 5)
		{
			while (existingFruit < maxExistingFruit)
			{
				FVector rndLoc = GetRandomPosition();
				GetWorld()->SpawnActor<AFruitBase>(fruitClass, rndLoc, FRotator());
				SpawnPortals();
				existingObjectLocations.Add(rndLoc);
				existingFruit++;
			}
		}
	}
}

FVector AGameFieldBase::GetRandomPosition()
{
	TArray<FVector> locationsToSpawn = ExcludeOccupaitedLocations();
	int rndIndex = FMath::RandRange(1, locationsToSpawn.Num() - 1);

	return locationsToSpawn[rndIndex];		
}

void AGameFieldBase::GotPortalAt(FVector location, FVector location2)
{
	existingPortal--;	
	existingObjectLocations.RemoveAt(existingObjectLocations.Find(location));
	existingObjectLocations.RemoveAt(existingObjectLocations.Find(location2));
}

void AGameFieldBase::GotFruitAt(FVector location)
{	
	existingFruit--;
	SpawnFruits();
	existingObjectLocations.RemoveAt(existingObjectLocations.Find(location));
}

void AGameFieldBase::FillLocations(int x, int y, float cellSize)
{
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			locations.Add(FVector(i * cellSize, j * cellSize, 60));	
		}
	}
	SpawnBorder(x, y);
}

TArray<FVector> AGameFieldBase::ExcludeOccupaitedLocations()
{
	TArray<FVector> freeLocations;
	for (int i = 0; i < locations.Num(); i++)
	{
		freeLocations.Add(locations[i]);
	}
	TArray<FVector> notFree = snakeHead->GetSnakeLocation();
	for (int i = 0; i < notFree.Num(); i++)
	{
		freeLocations.RemoveAt(freeLocations.Find(notFree[i]));
	}
	for (int i = 0; i < existingObjectLocations.Num(); i++)
	{
		freeLocations.RemoveAt(freeLocations.Find(existingObjectLocations[i]));
	}
	return freeLocations;
}