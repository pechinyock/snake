#include "SnakeHeadBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "SnakeElementBase.h"
#include "SnakeGamePlusPlus.h"
#include "GameFieldBase.h"
#include "SnakeGamePlusPlusGameModeBase.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"

float ASnakeHeadBase::movementSpeed = 0.25f;

ASnakeHeadBase::ASnakeHeadBase()
{
	PrimaryActorTick.bCanEverTick = true;

	isAlive = true;

	pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

	mesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	mesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeHeadBase::HandleBeginOverlap);

	RootComponent = mesh;
	elementSize = 110;

	newSnakeElementSpawnLocation = mesh->GetComponentLocation() - FVector(0, 0, -10000);

	forceToDie = 100.f;
}

void ASnakeHeadBase::BeginPlay()
{
	Super::BeginPlay();	
	SetActorTickInterval(movementSpeed);

	material = mesh->GetMaterial(0);
	UMaterialInstanceDynamic* dynamicMaterial = UMaterialInstanceDynamic::Create(material, this);
	dynamicMaterial->SetVectorParameterValue(TEXT("Colour"), FLinearColor::White);
	mesh->SetMaterial(0, dynamicMaterial);

	AddSnakeElememnt(3);
}

void ASnakeHeadBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();	
}

void ASnakeHeadBase::Move()
{
	if (isAlive)
	{
		isMoved = false;
		FVector previosSectionLoc = this->GetActorLocation();
		AddActorWorldOffset(movementDirection * elementSize);
		snakeLocation[0] = GetActorLocation();
		for (int i = 0; i < snakeElements.Num(); i++)
		{
			FVector currentElementLocation = snakeElements[i]->GetActorLocation();
			snakeElements[i]->SetActorLocation(previosSectionLoc);
			snakeLocation[i + 1] = previosSectionLoc;
			previosSectionLoc = currentElementLocation;
		}
	}	
}

void ASnakeHeadBase::AddSnakeElememnt(int elementsCount)
{	
	if (snakeLocation.Num() <= 0)
	{
		snakeLocation.Add(GetActorLocation());
	}
	
	for (int i = 0; i < elementsCount; i++)
	{
		FVector spawnLocation = newSnakeElementSpawnLocation - movementDirection * elementSize;
		FActorSpawnParameters spawnParamenters;
		spawnParamenters.Owner = this;
		spawnParamenters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ASnakeElementBase* newElement = GetWorld()->SpawnActor<ASnakeElementBase>(snakeElementClass, spawnLocation, FRotator(0, 0, 0), spawnParamenters);		

		if (newElement)
		{
			snakeElements.Add(newElement);
			snakeLocation.Add(spawnLocation);
			newSnakeElementSpawnLocation = newElement->mesh->GetComponentLocation();			
		}
	}	
	gameField = Cast<AGameFieldBase>(gameField);

	if(gameField)
	{
		gameField->GotFruitAt(GetActorLocation());
	}

	OnPickUpCubix();
}

void ASnakeHeadBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	if (IsValid(otherActor))
	{
		IInteractable* interactableInterface = Cast<IInteractable>(otherActor);
		if(interactableInterface)
		{
			interactableInterface->Interact(this);
		}
	}
}

int ASnakeHeadBase::GetSnakeElementsCount()
{
	return snakeElements.Num();
}

void ASnakeHeadBase::OnDie()
{
	isAlive = false;

	for (int i = 0; i < snakeElements.Num(); i++)
	{
		snakeElements[i]->mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		snakeElements[i]->mesh->SetSimulatePhysics(true);
		snakeElements[i]->mesh->AddForce(movementDirection * forceToDie);
		snakeElements[i]->mesh->AddLocalRotation(FRotator(GetRandomRotation(), GetRandomRotation(), GetRandomRotation()));
	}
	
	ASnakeGamePlusPlusGameModeBase* gameMode = Cast<ASnakeGamePlusPlusGameModeBase>(GetWorld()->GetAuthGameMode());
	gameMode->GameOver();

	this->Destroy();
}

void ASnakeHeadBase::OnPickUpCubix()
{
	pawn->SetScore(1);
}

float ASnakeHeadBase::GetMovementSpeed()
{
	return movementSpeed;
}

TArray<FVector> ASnakeHeadBase::GetSnakeLocation()
{
	return snakeLocation;
}

float ASnakeHeadBase::GetRandomRotation()
{
	return FMath::RandRange(0.f, 90.f);
}
