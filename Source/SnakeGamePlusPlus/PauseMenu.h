#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenu.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API UPauseMenu : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	class APlayerPawnBase* pawn;

	UPROPERTY(meta = (BindWidget))
	class UButton* restart;

	UPROPERTY(meta = (BindWidget))
	class UButton* resume;

	UPROPERTY(meta = (BindWidget))
	class UButton* quit;

private:

	virtual bool Initialize();

	UFUNCTION()
	void RestartGame();

	UFUNCTION()
	void ResuemGame();

	UFUNCTION()
	void QuitGame();	

public:

	UFUNCTION(BlueprintImplementableEvent)
	void OnResuemButtonClick();
};
