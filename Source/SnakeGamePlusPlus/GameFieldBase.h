#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFieldBase.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API AGameFieldBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameFieldBase();

	UPROPERTY()
	class ASnakeHeadBase* snakeHead;

	UPROPERTY()
	TArray<FVector> locations;

	UPROPERTY()
	TArray<FVector> existingObjectLocations;

	UPROPERTY()
	TArray<class ADeadZoneBase*> obstacles;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* mesh;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AFruitBase> fruitClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ADeadZoneBase> deadZoneClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class APoratalBase> portalClass;

	UPROPERTY()
	FTimerHandle timer;

	UPROPERTY()
	int borderX;

	UPROPERTY()
	int borderY;

protected:

	virtual void BeginPlay() override;	


public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawnBorder(int x, int y, int startX = 0, int startY = -1, float cellSize = 110.f);

	UFUNCTION()
	void SpawnFruits();

	UFUNCTION()
	FVector GetRandomPosition();

	UFUNCTION()
	void GotPortalAt(FVector location1, FVector location2);

	UFUNCTION()
	void GotFruitAt(FVector location);

	UFUNCTION()
	void FillLocations(int x = 10, int y = 10, float cellSize = 110.f);

	UFUNCTION()
	TArray<FVector> ExcludeOccupaitedLocations();

	UFUNCTION()
	void SpawnPortals();

	UFUNCTION()
	void RebuildGameField();

private:
	UPROPERTY()
	int maxExistingFruit;

	UPROPERTY()
	int existingFruit;

	UPROPERTY()
	int existingPortal;

	UPROPERTY()
	int maxExistingPortal;
};