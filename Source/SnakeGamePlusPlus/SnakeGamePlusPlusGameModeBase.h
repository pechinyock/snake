#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGamePlusPlusGameModeBase.generated.h"


UCLASS()
class SNAKEGAMEPLUSPLUS_API ASnakeGamePlusPlusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	FTimerHandle timer;	

public:

	void StartPlay() override;

	void GameOver();

	UFUNCTION()
	void RestartLevel();

	UFUNCTION()
	void Pause(bool bIsPaused);

private:


};
