#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GamePlayUI.generated.h"

UCLASS()
class SNAKEGAMEPLUSPLUS_API UGamePlayUI : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* scoreText;

	UPROPERTY(meta = (BindWidget))
	class UButton* pause;
		
	UPROPERTY()
	class APlayerPawnBase* pawn;

private:	

	virtual bool Initialize();

	UFUNCTION()
	FText SetScoreField();

	UFUNCTION()
	void SetGamePause();

public:

	UFUNCTION(BlueprintImplementableEvent)
	void OnPauseButtonClick();
};
